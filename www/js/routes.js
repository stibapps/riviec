
var routes = [
  {
    path: '/index/',
    url: './index.html',
  },
  {
    path: '/dashboard/',
    url: './dashboard.html',
  },
  // {
  //   path: '/dashboard/',
  //   url: './pages/dashboard.html',
  // },
  {
    path: '/about/',
    url: './pages/about.html',
  },
  {
    path: '/signin/',
    url: './pages/signin.html',
  },
  {
    path: '/signup/',
    url: './pages/signup.html',
  },
  {
    path: '/reset/',
    url: './pages/reset.html',
  },
  {
    path: '/form/',
    url: './form.html',
  },
  {
    path: '/alert/',
    url: './pages/alert.html',
  },
  {
    path: '/edit/',
    url: './pages/edit.html',
  },
  {
    path: '/editContact/',
    url: './pages/editContact.html',
  },
  {
    path: '/alerts/',
    url: './pages/alerts.html',
  },
  {
    path: '/alert-info/',
    url: './pages/alert-info.html',
  },
  {
    path: '/alerts-details/',
    url: './pages/alerts-details.html',
  },
  {
    path: '/guide/',
    url: './pages/guide.html',
  },
  {
    path: '/guide-info/',
    url: './pages/guide-info.html',
  },
  {
    path: '/actions/',
    url: './pages/actions.html',
  },
  {
    path: '/contact/',
    url: './pages/contact.html',
  },
  {
    path: '/create-contact/',
    url: './pages/create-contact.html',
  },
  {
    path: '/contacts/',
    url: './pages/contacts.html',
  },
  {
    path: '/contacts-info/',
    url: './pages/contacts-info.html',
  },
  {
    path: '/profile/',
    url: './pages/profile.html',
  },
  {
    path: '/messages/',
    url: './pages/messages.html',
  },
  {
    path: '/catalog/',
    componentUrl: './pages/catalog.html',
  },
  {
    path: '/alert/',
    componentUrl: './pages/alert.html',
  },
  {
    path: '/product/:id/',
    componentUrl: './pages/product.html',
  },
  {
    path: '/settings/',
    url: './pages/settings.html',
  },

  {
    path: '/dynamic-route/blog/:blogId/post/:postId/',
    componentUrl: './pages/dynamic-route.html',
  },
  {
    path: '/request-and-load/user/:userId/',
    async: function (routeTo, routeFrom, resolve, reject) {
      // Router instance
      var router = this;

      // App instance
      var app = router.app;

      // Show Preloader
      app.preloader.show();

      // User ID from request
      var userId = routeTo.params.userId;

      // Simulate Ajax Request
      setTimeout(function () {
        // We got user data from request
        var user = {
          firstName: 'Vladimir',
          lastName: 'Kharlampidi',
          about: 'Hello, i am creator of Framework7! Hope you like it!',
          links: [
            {
              title: 'Framework7 Website',
              url: 'http://framework7.io',
            },
            {
              title: 'Framework7 Forum',
              url: 'http://forum.framework7.io',
            },
          ]
        };
        // Hide Preloader
        app.preloader.hide();

        // Resolve route to load page
        resolve(
          {
            componentUrl: './pages/request-and-load.html',
          },
          {
            context: {
              user: user,
            }
          }
        );
      }, 1000);
    },
  },
  // Default route (404 page). MUST BE THE LAST
  {
    path: '(.*)',
    url: './pages/404.html',
  },
];
