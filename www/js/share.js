const shareBtn = document.querySelector('.shareBtn a');

shareBtn.addEventListener('click', () => {
  if (navigator.share) {
    navigator.share({
      title: 'RIVIEC',
      text: 'Checkout the Riviec App',
      url: window.location.href
    }).then(() => {
      console.log('Thanks for sharing!');
    })
    .catch(err => {
      console.log(`Couldn't share because of`, err.message);
    });
  } else {
    console.log('web share not supported');
  }
});